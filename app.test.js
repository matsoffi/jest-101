const { fetchSomeData } = require('./app')
// it same test
it('should works with async', async () => {
    const response = await fetchSomeData()
    expect(response).toEqual('success')
})

it('should works with resolves', async () => {
    await expect(fetchSomeData()).resolves.toEqual('success')
})
  
it('should works with jest.fn() and mockResolvedValue', async () => {
    let myMock = fetchSomeData
    myMock = jest.fn()
    myMock.mockResolvedValue('success from mock data')
    await expect(myMock()).resolves.toEqual('success from mock data')
})