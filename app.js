exports.getScore = score => {
    if (score < 50) {
        return 'F'
    }
    return 'A'
}

exports.fetchSomeData = () => {
    return new Promise(resolve => {
        setTimeout(() => resolve('success'), 3000)
    })
}

exports.login = async () => {
    const response = await fetchSomeData()
    return response === 'success'
}